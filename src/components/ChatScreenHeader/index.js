import { StyleSheet, Text, View, StatusBar } from 'react-native'
import React, {useState} from 'react'

const ChatHeader = (props) => {

    const { data } = props;
    const [lastSeen, setlastSeen] = useState('')

    return (
        <View style={styles.container}>
            <View
                style={{ flex: 1, marginLeft: 10 }}>
                <Text
                    numberOfLines={1}
                    style={{
                        fontSize: 16,
                        textTransform: 'capitalize'
                    }}>
                </Text>
            </View>
        </View>
    )
}

export default ChatHeader

const styles = StyleSheet.create({
    container: {
        height: 70,
        backgroundColor: '#27AE60',
        elevation: 5,
        flexDirection: 'row',
        alignItems: 'center',
    },
})