import { LOGIN_SUCCESS} from './types';

export const LoginSuccess = payload => ({
    type: LOGIN_SUCCESS,
    payload: payload
  });
  