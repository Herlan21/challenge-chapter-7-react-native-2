import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native'
import React from 'react'
import { ChatHeader } from '../../components'
import Icon from 'react-native-vector-icons/Ionicons'

const ChatScreen = () => {
  return (
    <View style={styles.container}>
      <ChatHeader />

      <Image style={styles.bg}
        source={require('../../Assets/bg.jpg')} />

      <View  style={styles.wrapper}>
        <TextInput
          style={styles.type}
          placeholder="type a message"
          multiline={true}
        />
      </View>

      <View>
       <TouchableOpacity style={styles.sendIcon} onPress={() => props.navigation.navigate('AllUser')}>
        <Icon name="paper-plane-sharp" size={30} color="black"/>
      </TouchableOpacity>
      </View>
    </View>
  )
}

export default ChatScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  bg: {
    flex: 1,
  },

  type: {
    backgroundColor: '#fff',
    width: '80%',
    borderRadius: 25,
    borderWidth: 0.5,
    borderColor: '#fff',
    paddingHorizontal: 15,
    color: '#000',
    
  },

  sendIcon: {
    backgroundColor: '#27AE60',
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    position: 'absolute',
    right: 5,
    bottom: 15,
  },

  wrapper: {
    bottom: 18,
    left: 10,
    
  }
  


})