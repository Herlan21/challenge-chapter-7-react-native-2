import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity } from 'react-native'
import React, { useState, useEffect } from 'react'
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux'
import SearchBar from "react-native-dynamic-search-bar";
import database from '@react-native-firebase/database';
import uuid from 'react-native-uuid';

const AllUserScreen = () => {
  const navigation = useNavigation();

  const [allUser, setAllUser] = useState([])
  const [allUserBackup, setAllUserBackup] = useState([])

  const [search, setSearch] = useState('')


  const userData = useSelector((state) => state.userData)
  console.log('userData: ', userData);

  useEffect(() => {
    getUser()
  }, [])

  const getUser = () => {
    database()
      .ref('/users/')
      .once('value')
      .then(snapshot => {
        console.log('User data: ', snapshot.val());
        console.log('Object Values: ', Object.values(snapshot.val()))

        setAllUser(Object.values(snapshot.val()).filter(it => it.id != userData.id))
        setAllUserBackup(Object.values(snapshot.val()).filter(it => it.id != userData.id))
      });
  }

  const searchUser = val => {
    setSearch(val);
    setAllUser(allUserBackup.filter(it => it.name.match(val)));
  };

  const chatList = data => {
    // database()
    //   .ref('/chatlist/' + userData.id + "/" + data.id)
    //   .once('value')
    //   .then(() => {
    //     console.log('User Data: ', snapshot.val());

        // if (snapshot.val() == null) {
          let roomId = uuid.v4()
          let myData = {
            roomId,
            id: userData.id,
            name: userData.name,
            avatar: userData.avatar,
            emailId: userData.emailId,
            bio: userData.bio,
            lastMsg: ""
          }

          database()
            .ref('/chatlist/' + data.id + "/" + userData.id)
            .update(myData)
            .then(() => {
              console.log('Data updated');
            });

          delete data['password']
          data.lasMsg = ""
          data.roomId = roomId

          database()
            .ref('/chatlist/' + userData.id + "/" + data.id)
            .update(data)
            .then(() => {
              console.log('Data updated');
            });
          navigation.navigate('Chat');
        // } else {
        //   navigation.navigate('Chat', { receiverData: snapshot.val() });
        // }
      // });
  }



  const renderItem = ({ item }) => (
    <TouchableOpacity style={styles.wrapper} onPress={() => chatList(item)}>
      <Image style={styles.avatar} source={{ uri: item.avatar }} />

      <View style={{ marginLeft: 15 }}>
        <Text style={styles.name}>{item.name}</Text>
        <Text style={styles.bio}>{item.bio}</Text>
      </View>
    </TouchableOpacity>
  )

  return (
    <View>
      <SearchBar
        style={styles.search}
        placeholder="Search here"
        onPress={() => alert("Please insert name")}
        onChangeText={(text) => searchUser(text)}
        value={search}
      />

      <FlatList
        showsHorizontalScrollIndicator={false}
        inverted
        data={allUser}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}

export default AllUserScreen

const styles = StyleSheet.create({

  wrapper: {
    marginTop: 10,
    elevation: 2,
    marginHorizontal: 12,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },

  avatar: {
    width: 75,
    height: 72,
    borderRadius: 40
  },

  name: {
    marginTop: 5,
    color: '#000',
    fontSize: 18,
    fontFamily: 'monospace'
  },

  bio: {
    color: '#000',
    marginTop: 2,
    fontSize: 14,
    fontFamily: 'monospace'
  },

  search: {
    marginTop: 15
  }
})