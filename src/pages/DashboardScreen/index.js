import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, Image } from 'react-native';
import { useSelector } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons';
import database from '@react-native-firebase/database'

const DasboardScreen = (props) => {

    const userData = useSelector((state) => state.userData)
    console.log('userData: ', userData);

    const [chatList, setChatList] = useState([])

    useEffect(() => {
        getChatList()
    }, [])

    const getChatList = async () => {
        database()
            .ref('/chatlist/' + userData?.id)
            .on('value', snapshot => {
                // console.log('User data: ', Object.values(snapshot.val()))
                if (snapshot.val() != null) {
                    setChatList(Object.values(snapshot.val()))
                }
            });
    }
    console.log('chatList', chatList);

    const renderItem = ({ item }) => (
        <TouchableOpacity style={styles.wrapper} onPress={() => props.navigation.navigate('Chat', { receiverData: item })}>
            <Image style={styles.avatar} source={{ uri: item.avatar }} />

            <View style={{ marginLeft: 15 }}>
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.bio}>{item.lastMsg}</Text>
            </View>
        </TouchableOpacity>
        
    )

    return (

        <View style={{ flex: 1 }}>
            <View style={styles.container}>
                <Text style={styles.dashboard}>Dashboard</Text>
                <Icon name="notifications" size={30} color="green" />
            </View>

            <FlatList
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                data={chatList}
                renderItem={renderItem}
            />

            <TouchableOpacity style={styles.user} onPress={() => props.navigation.navigate('AllUser')}>
                <Icon name="people" size={30} color="white" />
            </TouchableOpacity>
        </View>


    )
}

export default DasboardScreen

const styles = StyleSheet.create({

    name: {
        marginTop: 5,
        color: '#000',
        fontSize: 18,
        fontFamily: 'monospace'
      },

    avatar: {
        width: 75,
        height: 72,
        borderRadius: 40
      },

    wrapper: {
        marginTop: 10,
        elevation: 2,
        marginHorizontal: 12,
        flexDirection: 'row',
        backgroundColor: '#fff',
      },

    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        elevation: 2,
        paddingVertical: 15,
        // position: 'relative',
    },

    dashboard: {
        color: '#27AE60',
        fontSize: 22,
    },

    user: {
        backgroundColor: '#27AE60',
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        position: 'absolute',
        right: 15,
        bottom: 15,

    },

    name: {
        marginTop: 5,
        color: '#000',
        fontSize: 18,
        fontFamily: 'monospace'
      },
    
})